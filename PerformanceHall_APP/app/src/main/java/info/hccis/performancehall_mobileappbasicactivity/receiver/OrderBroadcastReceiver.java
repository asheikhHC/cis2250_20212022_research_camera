package info.hccis.performancehall_mobileappbasicactivity.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class OrderBroadcastReceiver extends BroadcastReceiver {
    public OrderBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Action: " + intent.getAction(), Toast.LENGTH_SHORT).show();
        Log.d("BJM receiver","Order Created...Action: " + intent.getAction());
    }
}