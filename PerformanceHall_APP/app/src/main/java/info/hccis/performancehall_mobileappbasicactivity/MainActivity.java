package info.hccis.performancehall_mobileappbasicactivity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.room.FtsOptions;
import androidx.room.Room;

import info.hccis.performancehall_mobileappbasicactivity.api.ApiWatcher;
import info.hccis.performancehall_mobileappbasicactivity.dao.MyAppDatabase;
import info.hccis.performancehall_mobileappbasicactivity.databinding.ActivityMainBinding;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;
import info.hccis.performancehall_mobileappbasicactivity.receiver.OrderBroadcastReceiver;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

/**
 * The main activity is used for the core business functionality for this app.  It contains a location
 * to have a fragment on it's view.
 * <p>
 * The main activity does not need to change much.  The menu and floating action button code is defined here
 * but the add/view order coding is contained in the fragments.
 *
 * @author BJM
 * @since 20220129
 */

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private static ApiWatcher apiWatcher;
    private OrderBroadcastReceiver orderBroadcastReceiver = new OrderBroadcastReceiver();





    //Room Database
    private static MyAppDatabase myAppDatabase;

    //Provide a getter for the database to be used throughout the app.
    public static MyAppDatabase getMyAppDatabase() {
        return myAppDatabase;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        //************************************************************************************
        // Listener for FAB here
        //************************************************************************************

//        binding.fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });




        //fab button for camera
        FloatingActionButton fab2 = findViewById(R.id.fab2);

        //fab click listener for camera
        binding.fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                startActivity(new Intent(getApplicationContext(),CameraActivity.class));

            }
        });


        apiWatcher = new ApiWatcher();
        apiWatcher.setActivity(this);
        apiWatcher.start();  //Start the background thread

        //****************************************************************************************
        //Set the database attribute
        //****************************************************************************************
        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "performancehalldb").allowMainThreadQueries().build();

        //**************************************************************************************
        //register receiver
        //**************************************************************************************
        IntentFilter filter = new IntentFilter();
        filter.addAction("info.hccis.phall.order");
        //registerReceiver(exampleReceiver, filter);
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        lbm.registerReceiver(orderBroadcastReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        Log.d("BJM receiver", "unregistering");
        lbm.unregisterReceiver(orderBroadcastReceiver);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Code here for reacting to user selecting menu items.
     *
     * @param item
     * @return true/false
     * @author BJM
     * @since 20220129
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.d("MainActivity BJM", "Option selected Settings");
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_about) {
            Log.d("MainActivity BJM", "Option selected About");
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }





}