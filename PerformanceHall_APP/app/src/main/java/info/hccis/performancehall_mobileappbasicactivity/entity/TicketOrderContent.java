package info.hccis.performancehall_mobileappbasicactivity.entity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import info.hccis.performancehall_mobileappbasicactivity.MainActivity;
import info.hccis.performancehall_mobileappbasicactivity.R;
import info.hccis.performancehall_mobileappbasicactivity.ViewOrdersFragment;
import info.hccis.performancehall_mobileappbasicactivity.api.ApiWatcher;
import info.hccis.performancehall_mobileappbasicactivity.api.JsonTicketOrderApi;
import info.hccis.performancehall_mobileappbasicactivity.util.NotificationUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TicketOrderContent {

    /**
     * This method will take the list passed in and reload the room database
     * based on the items in the list.
     * @param ticketOrders
     * @since 20220210
     * @author BJM
     */
    public static void reloadTicketOrdersInRoom(List<TicketOrder> ticketOrders)
    {
        MainActivity.getMyAppDatabase().ticketOrderDAO().deleteAll();
        for(TicketOrder current : ticketOrders)
        {
            MainActivity.getMyAppDatabase().ticketOrderDAO().insert(current);
        }
        Log.d("BJM Room","loading ticket orders into Room");
    }


    /**
     * This method will obtain all the ticket orders out of the Room database.
     * @return list of ticket orders
     * @since 20220210
     * @author BJM
     */
    public static List<TicketOrder> getTicketOrdersFromRoom()
    {
        Log.d("BJM Room","Loading ticket orders from Room");

        List<TicketOrder> ticketOrdersBack = MainActivity.getMyAppDatabase().ticketOrderDAO().selectAllTicketOrders();
        Log.d("BJM Room","Number of ticket orders loaded from Room: " + ticketOrdersBack.size());
        for(TicketOrder current : ticketOrdersBack)
        {
            Log.d("BJM Room",current.toString());
        }
        return ticketOrdersBack;
    }

}